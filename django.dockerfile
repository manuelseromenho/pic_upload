FROM python:alpine

RUN mkdir ./project
VOLUME ["./project"]
RUN pip install django==1.11
WORKDIR ./project
CMD ['bash']
