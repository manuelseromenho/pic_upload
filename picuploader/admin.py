from django.contrib import admin

from picuploader.models import Picture

admin.site.register(Picture)