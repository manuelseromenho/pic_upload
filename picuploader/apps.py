from django.apps import AppConfig


class PicuploaderConfig(AppConfig):
    name = 'picuploader'
