from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import post_delete
from django.dispatch import receiver


class Picture(models.Model):
    name= models.CharField(max_length=500)
    picturefile= models.ImageField(upload_to='images/', null=True, verbose_name="")

    def __str__(self):
        return str(self.picturefile)

    def get_absolute_url(self):
        return reverse("picuploader:picture_detail", kwargs={"pk": self.pk})


@receiver(post_delete, sender=Picture)
def submission_delete(sender, instance, **kwargs):
    """
        Deletes the file relative to the picture object that is deleted.
    """
    instance.picturefile.delete(False)