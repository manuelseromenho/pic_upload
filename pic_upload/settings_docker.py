from pic_upload.settings import *

PREPEND_WWW = False
DEBUG = True

PROJECT_URL = 'http://0.0.0.0:8000'


MEDIA_URL= '%s/static/' % PROJECT_URL
MEDIA_ROOT= os.path.join(BASE_DIR, 'media')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = [
    os.path.join(APPLICATION_ROOT, 'static'),
    os.path.join(BASE_DIR, 'media'),
]


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pic_upload_db',
        'USER': 'root',
        'PASSWORD': 'qwertyuiop',
        'HOST': 'mysql',
        'PORT': '',
        'OPTIONS': {
            'charset': 'utf8',
            'init_command': 'SET default_storage_engine=MYISAM, '
                            'sql_mode=NO_ENGINE_SUBSTITUTION, '
                            'character_set_connection=utf8, '
                            'collation_connection=utf8_bin;',
        },
    }
}

EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_HOST_USER = 'e5c7f6b8c32193'
EMAIL_HOST_PASSWORD = '35ad262eedfd8a'
EMAIL_PORT = '2525'