from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (TemplateView, ListView, CreateView, UpdateView, DetailView, DeleteView)

from .models import Picture
from .forms import PictureForm


def upload_picture(request):

    form = PictureForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()

        return redirect('picuploader:picture_success')

    return render(request, 'picture_upload.html', {'form': form})


def show_picture_uploaded(request):
    lastpicture = Picture.objects.last()
    picture = lastpicture.picturefile

    context = {
        'picture': picture,
    }

    return render(request, 'success.html', context)


def show_pictures(request):
    pictures = Picture.objects.all()

    context = {
        'pictures': pictures
    }

    return render(request, 'pics.html', context)


class PictureDetailView(DetailView):
    model = Picture


class PictureDeleteView(DeleteView):
    model = Picture
    success_url = reverse_lazy("picuploader:show_pictures")