from django.conf.urls import url

from picuploader import views

app_name = "picuploader"

urlpatterns = [
    url(r'upload/$', views.upload_picture, name="upload_picture"),
    url(r'^upload/success/$', views.show_picture_uploaded, name="picture_success"),
    url(r'^picture/(?P<pk>\d+)$', views.PictureDetailView.as_view(), name="picture_detail"),
    url(r'^picture/remove/(?P<pk>\d+)$', views.PictureDeleteView.as_view(), name="picture_remove"),
    url(r'$', views.show_pictures, name="show_pictures"),
]
