FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN apt-get update --fix-missing
RUN apt-get install -y \
    python-pip \
    python-dev \
    libpq-dev \
    gettext \
    libreadline-dev \
    libssl-dev \
    libjpeg-dev \
    libfreetype6-dev \
    binutils \
    libproj-dev \
    gdal-bin \
    postgis

RUN apt-get update
RUN pip install --upgrade pip

RUN mkdir /code
ADD . /code/

WORKDIR /code/

RUN pip install -r requirements/docker.pip